package com.particle.device;

public class Photon {

	private String name;
	private String id;
	private boolean needsUpdate;
	private boolean updated;
	private String firmwareFilePath;
	
	public Photon(){}
	
	public Photon(String name, String id, boolean needsUpdate, boolean updated, String firmwareFilePath) {
		this.name = name;
		this.id = id;
		this.needsUpdate = needsUpdate;
		this.updated = updated;
		this.firmwareFilePath=firmwareFilePath;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isNeedsUpdate() {
		return needsUpdate;
	}
	public void setNeedsUpdate(boolean needsUpdate) {
		this.needsUpdate = needsUpdate;
	}
	public boolean isUpdated() {
		return updated;
	}
	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

	public String getFirmwareFilePath() {
		return firmwareFilePath;
	}

	public void setFirmwareFilePath(String firmwareFilePath) {
		this.firmwareFilePath = firmwareFilePath;
	}
	
	

}
