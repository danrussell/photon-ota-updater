package com.particle.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.particle.device.Photon;

@Configuration
@ComponentScan(basePackages="com.particle")
@EnableWebMvc
public class AppConfig extends WebMvcConfigurerAdapter{
	
	@Autowired
	private ApplicationContext appContext;
	
	@Value("#{${devices}}") 
	private Map<String,String> devices;
	
	@Bean
    public ViewResolver getViewResolver(){
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        resolver.setCharacterEncoding("UTF-8");
        return resolver;
    }
	
	@Bean(name="devicesMap")
	public Map<String,Photon> deviceMap(){
		Map<String,Photon> devicesMap = new HashMap<String,Photon>();
		
		for(Map.Entry<String, String> entry : devices.entrySet()){
			devicesMap.put(entry.getKey(), new Photon(entry.getKey(), entry.getValue(), false, false, ""));
		}
		
		return devicesMap;
	}
	
	private SpringTemplateEngine templateEngine(){
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.setTemplateResolver(templateResolver());
		return engine;
	}
	
	private ITemplateResolver templateResolver(){
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext(appContext);
		resolver.setPrefix("/WEB-INF/templates/");
		resolver.setTemplateMode("HTML5");
		return resolver;
	}
	
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**/*").addResourceLocations("/resources/");
    }
}
