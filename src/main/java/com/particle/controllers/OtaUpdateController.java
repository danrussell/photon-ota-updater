package com.particle.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.particle.device.Photon;

@RestController
public class OtaUpdateController {
	
	@Resource(name="devicesMap")
	private Map<String,Photon> devicesMap;

	@RequestMapping("/devices")
	public ModelAndView devicesStatus(){
		ModelAndView mv = new ModelAndView("allDevices");
		mv.addObject("devices", devicesMap);
		return mv;
	}
	
	@RequestMapping("/editDevice/{deviceName}")
	public ModelAndView editDevice(@PathVariable("deviceName") String deviceName){
		ModelAndView mv = new ModelAndView("editDevice");
		mv.addObject("device", devicesMap.get(deviceName));
		return mv;
	}
	
	@RequestMapping("/updateDeviceConfig/{deviceName}")
	public ModelAndView updateDeviceConfig(@PathVariable("deviceName") String deviceName){
		ModelAndView mv = new ModelAndView("redirect:/editDevice/"+deviceName);
		Photon p = devicesMap.get(deviceName);
		p.setNeedsUpdate(true);
		mv.addObject("device", p);
		return mv;
	}
	
	@RequestMapping(value="/updateDeviceConfig/{deviceName}", method=RequestMethod.POST)
	public ModelAndView updateDeviceConfig2(@PathVariable("deviceName") String deviceName, @RequestParam("firmwareFile") String firmwarePath){
		ModelAndView mv = new ModelAndView("redirect:/editDevice/"+deviceName);
		Photon p = devicesMap.get(deviceName);
		p.setNeedsUpdate(true);
		p.setFirmwareFilePath(firmwarePath);
		mv.addObject("device", p);
		return mv;
	}
	
	@RequestMapping(value="/updateRequired/{deviceName}")
	public String update(@PathVariable("deviceName") String deviceName){
		String retVal="no";
		Photon device = devicesMap.get(deviceName);
		if(device.isNeedsUpdate() && verifyFirmwareExists(device.getFirmwareFilePath())){
			retVal= device.isUpdated() ? "no" : "yes";
		}
		return retVal;
	}
	
	@RequestMapping(value="/doUpdate/{deviceName}")
	public String doUpdate(@PathVariable("deviceName") String deviceName){
		String retVal="Failed";
		
		Photon device = devicesMap.get(deviceName);
		
		if(verifyFirmwareExists(device.getFirmwareFilePath())){
			String line = String.format("particle flash %s %s",device.getId(),device.getFirmwareFilePath());
			CommandLine commandLine = CommandLine.parse(line);
			DefaultExecutor executor = new DefaultExecutor();
			executor.setExitValue(0);
			try {
				int exitValue = executor.execute(commandLine);
				
				if(exitValue==0){
					retVal = deviceName+" updated successfully";
					device.setNeedsUpdate(false);
				}else{
					retVal = deviceName+" did not update properly";
				}
				  
				//TODO: log statement
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			retVal=device.getFirmwareFilePath()+" could not be found on the file system!";
		}
		
		return retVal;
	}
	
	private boolean verifyFirmwareExists(String path){
		if(path!=null){
			File firmware = new File(path);
			return firmware.exists();
		}else{
			return false;
		}
	}
}
