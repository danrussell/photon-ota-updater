// This #include statement was automatically added by the Particle IDE.
#include "HttpClient/HttpClient.h"


/***********************
    THIS WILL SUCCESSFULLY UPDATE A PHOTON WHEN TRIGGERED BY SPRING BOOT 
***************************/

unsigned int nextTime = 0;    // Next time to contact the server
HttpClient http;
String deviceName="not_set";

// Headers currently need to be set at init, useful for API keys etc.
http_header_t headers[] = {
    //  { "Content-Type", "application/json" },
    //  { "Accept" , "application/json" },
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;

void setup() {
    Serial.begin(9600);
    Particle.subscribe("spark/", handler);
    Particle.publish("spark/device/name");
}

void loop() {
    if (nextTime > millis()) {
        return;
    }

    Serial.println();
    Serial.println("Application>\tStart of Loop. Device name is: " + deviceName);
    // Request path and body can be set at runtime or at setup.
    request.hostname = "198.168.0.1";
    request.port = 8080;
    request.path = "/updateRequired/"+deviceName;

    // The library also supports sending a body with your request:
    //request.body = "{\"key\":\"value\"}";

    // Get request
    http.get(request, response, headers);
    Serial.print("Application>\tResponse status: ");
    Serial.println(response.status);

    Serial.print("Application>\tHTTP Response Body: ");
    Serial.println(response.body);
    
    if(response.body=="yes"){
        Serial.println("Updating...");
        request.path = "/doUpdate/"+deviceName;
        http.get(request, response, headers);
        Serial.println(response.body);
        delay(10000);
    }
    nextTime = millis() + 10000;
}

void handler(const char *topic, const char *data) {
    deviceName=String(data);
}

